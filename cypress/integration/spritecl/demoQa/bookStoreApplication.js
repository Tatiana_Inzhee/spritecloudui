describe('Search Test Suite', function () {
    it('I can successfully register in the book store', function () {

        cy.visit("https://demoqa.com/"); //Visit the Demo QA Website
        cy.get('.category-cards > div:nth-of-type(6)').click(); // Clicking on the book store item
        cy.get('.accordion div:nth-of-type(6) div.element-list ul.menu-list > li').should('have.length',4);
        cy.get('.accordion div:nth-of-type(6) div.element-list ul.menu-list li:nth-of-type(1)').click();
        cy.get('#userName').type('Tati_Injee');
        cy.get('#password').type('Tati4Injeeeret&');
        cy.get('#login').click();
    });
})