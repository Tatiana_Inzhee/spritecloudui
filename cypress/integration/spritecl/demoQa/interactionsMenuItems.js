describe('Search Test Suite', function () {
    it('Search Items under Interaction Tab', function () {

        cy.visit("https://demoqa.com/"); //Visit the Demo QA Website
        cy.get('.category-cards > div:nth-of-type(5)').click(); // Clicking on Widget Menu Item
        //Verify number of items present on Interaction Tab
        cy.get('.accordion div:nth-of-type(5) div.element-list ul.menu-list > li').should('have.length',5);
    });
})