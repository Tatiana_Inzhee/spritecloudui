describe('Search Test Suite', function () {
    it('Search Items under Widgets Tab', function () {

        cy.visit("https://demoqa.com/"); //Visit the Demo QA Website
        cy.get('.category-cards > div:nth-of-type(1)').click(); // Clicking on Widget Menu Item
        cy.get('.accordion div:nth-of-type(1) div.element-list ul.menu-list > li').should('have.length',9);
    });
})